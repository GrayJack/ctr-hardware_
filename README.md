# ctr-hardware
A library to communicate with the 3DS hardware.

## Using it
Add this to your `Cargo.toml` dependencies:
```
ctr-hardware = { git = "https://gitlab.com/GrayJack/ctr-hardware_" }
```

OR when the github one is public:
```
ctr-hardware = { git = "https://github.com/GrayJack/ctr-hardware" }
```

Then just load it in your crate:
**2015:**
```rust
extern crate ctr_hardware;

use ctr_hardware;
```

**2018:**
```rust
use ctr_hardware;
```

## TODO
 * Add docs links

## License
This Source Code Form is subject to the terms of the Mozilla Public License, v.
2.0. If a copy of the MPL was not distributed with this file, You can obtain one
at http://mozilla.org/MPL/2.0/.

This Source Code Form is "Incompatible With Secondary Licenses", as defined by
the Mozilla Public License, v. 2.0.
