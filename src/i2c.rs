//! Inter Integrated Circuit (I²C) module and API
//!
//! Detailed information on [3dbrew](https://www.3dbrew.org/wiki/I2C_Registers)

use super::Register;

pub const I2C_BUS_1_BASE: u32 = 0x10161000;
pub const I2C_BUS_2_BASE: u32 = 0x10144000;
pub const I2C_BUS_3_BASE: u32 = 0x10148000;

/// I²C Devices on the 3DS family products.
///
/// Aditional information on [3dbrew](https://www.3dbrew.org/wiki/I2C_Registers#I2C_Devices)
#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum Device {
    /// Micro Controler Unit (MCU). Maybe power management (same device addr as the DSi
    /// power-management).
    Mcu0 = 0x00,
    /// Camera 0.
    Cam0 = 0x01,
    /// Camera 1.
    Cam1 = 0x02,
    /// Micro Controler Unit (MCU). For sure power management.
    Mcu1 = 0x03,
    /// Camera 2.
    Cam2 = 0x04,
    /// Liquid Crystal Display 0 (LCD0).
    Lcd0 = 0x05,
    /// Liquid Crystal Display 1 (LCD1).
    Lcd1 = 0x06,
    /// Unknown
    Deb0 = 0x07,
    /// Unknown
    Deb1 = 0x08,
    /// Human Interface Device 0 (HID0). Unknown
    Hid0 = 0x09,
    /// Human Interface Device 1 (HID1). Gyroscope (Old3DS)
    Hid1 = 0x0A,
    /// Human Interface Device 2 (HID2). Gyroscope (Old2DS, New3DS, New2DS)
    Hid2 = 0x0B,
    /// Human Interface Device 3 (HID3). Debug Pad.
    Hid3 = 0x0C,
    /// InfraRed 0.
    Ir0  = 0x0D,
    /// EEPROM. Only present on dev units.
    Eep  = 0x0E,
    /// Near Field Communication (NFC). Only exist on New3DS/New2DS models.
    Nfc  = 0x0F,
    /// QTM. Only exist on New3DS/New2DS models.
    Qtm  = 0x10,
    /// Infra Red 1.
    Ir1  = 0x11,
}

impl Device {
    /// Get the device ID.
    pub const fn device_id(self) -> u8 { self as u8 }

    /// Get the ID of the BUS for the `Device`.
    pub const fn bus_id(self) -> u8 {
        use Device::*;
        match self {
            Mcu0 | Cam0 | Cam1 | Qtm => 1,
            Mcu1 | Cam2 | Lcd0 | Lcd1 | Deb0 | Deb1 | Nfc => 2,
            Hid0 | Hid1 | Hid2 | Hid3 | Ir0 | Eep | Ir1 => 3,
        }
    }

    /// Get the write address for the `Device`.
    pub const fn write_address(self) -> u8 {
        match self {
            Self::Mcu0 => 0x4A,
            Self::Cam0 => 0x7A,
            Self::Cam1 => 0x78,
            Self::Mcu1 => 0x4A,
            Self::Cam2 => 0x78,
            Self::Lcd0 => 0x2C,
            Self::Lcd1 => 0x2E,
            Self::Deb0 => 0x40,
            Self::Deb1 => 0x44,
            Self::Hid0 => 0xD6,
            Self::Hid1 => 0xD0,
            Self::Hid2 => 0xD2,
            Self::Hid3 => 0xA4,
            Self::Ir0 => 0x9A,
            Self::Eep => 0xA0,
            Self::Nfc => 0xEE,
            Self::Qtm => 0x40,
            Self::Ir1 => 0x54,
        }
    }

    /// Get the base adress for the `Device`.
    const fn base(&self) -> u32 {
        match self.bus_id() {
            1 => I2C_BUS_1_BASE,
            2 => I2C_BUS_2_BASE,
            3 => I2C_BUS_3_BASE,
            _ => unreachable!(),
        }
    }

    /// Get `CNT` register of the device.
    pub fn cnt(self) -> CntInfo { CntInfo(self.raw_cnt()) }

    /// Get `DATA` register of the device in raw `u8` value.
    pub fn raw_data(self) -> u8 { self.read_as_u8_with(I2CRegisterType::Data) }

    /// Get `CNT` register of the device in raw `u8` value.
    pub fn raw_cnt(self) -> u8 { self.read_as_u8_with(I2CRegisterType::Cnt) }

    /// Get `CNTEX` register of the device in raw `u8` value.
    pub fn raw_cntex(self) -> u8 { self.read_as_u8_with(I2CRegisterType::Cntex) }

    /// Get `SCL` register of the device in raw `u8` value.
    pub fn raw_scl(self) -> u8 { self.read_as_u8_with(I2CRegisterType::Scl) }

    pub fn wait_busy(self) {
        while self.cnt().is_running() {}
    }
}

impl Register for Device {
    type AdditionalInfo = I2CRegisterType;

    fn address_with(&self, info: Self::AdditionalInfo) -> u32 { self.base() + info as u32 }
}

/// Register type of the `Device`s.
#[derive(Debug, Clone, Copy)]
#[repr(u32)]
pub enum I2CRegisterType {
    Data  = 0x00,
    Cnt   = 0x01,
    Cntex = 0x02,
    Scl   = 0x04,
}

/// `CNT` information in a more structured form.
#[derive(Debug, Clone, Copy)]
pub struct CntInfo(u8);

impl CntInfo {
    /// Returns `true` if `CNT` register is in stop state
    pub fn is_stop(self) -> bool { (self.0 & (1 << 7)) != 0 }

    /// Returns `true` if `CNT` register is in start state
    pub fn is_start(self) -> bool { (self.0 & (1 << 6)) != 0 }

    /// Returns `true` if `CNT` register is in pause state
    pub fn is_pause(self) -> bool { (self.0 & (1 << 5)) != 0 }

    /// Get `CNT` `Ack` state.
    pub fn ack(self) -> Ack { if (self.0 & (1 << 4)) == 0 { Ack::Error } else { Ack::Ok } }

    /// Get `CNT` data direction
    pub fn data_direction(self) -> DataDirection {
        if (self.0 & (1 << 2)) == 0 { DataDirection::Write } else { DataDirection::Read }
    }

    /// Returns `true` if `CNT` register have interrupts enabled.
    pub fn is_interrupt_enable(self) -> bool { (self.0 & (1 << 1)) != 0 }

    /// Returns `true` if the `CNT` state is busy (a.k.a. running).
    pub fn is_running(self) -> bool { (self.0 & (1 << 0)) != 0 }
}

/// `CNT` data direction.
///
/// It can be either `Write` or `Read`.
#[derive(Debug, Copy, Clone, PartialEq)]
#[repr(u8)]
pub enum DataDirection {
    Write = 0,
    Read  = 1,
}

/// `CNT` ack representation.
///
/// It can be either `Error` or `Ok`.
#[derive(Debug, Copy, Clone, PartialEq)]
#[repr(u8)]
pub enum Ack {
    Error = 0,
    Ok    = 1,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn cnt_info() {
        let cnt = CntInfo(0b11110111);
        assert!(cnt.is_stop());
        assert!(cnt.is_pause());
        assert!(cnt.is_start());
        assert!(cnt.is_interrupt_enable());
        assert!(cnt.is_running());
        assert_eq!(cnt.ack(), Ack::Ok);
        assert_eq!(cnt.data_direction(), DataDirection::Read);
    }
}
