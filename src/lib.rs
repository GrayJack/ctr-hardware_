#![feature(associated_type_defaults)]
#![feature(const_if_match)]
#![feature(const_panic)]
#![feature(const_loop)]
#![feature(const_fn)]
#![no_std]

pub mod i2c;
pub mod irq;

/// Specify that a object (usually `enum`) is a register in memory.
pub trait Register {
    /// Aditional information to get the address. Defaults to `()`, unit type.
    type AdditionalInfo: Copy = ();

    /// The address of the register when aditional info required.
    fn address_with(&self, info: Self::AdditionalInfo) -> u32;

    /// The address of the register when no aditional info required.
    fn address(&self) -> u32
    where
        Self: Register<AdditionalInfo = ()>,
    {
        self.address_with(())
    }

    /// Returns a imutable pointer of a `Register` object.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `T`
    fn as_ptr_with<T: Copy>(&self, info: Self::AdditionalInfo) -> *const T {
        assert!((self.address_with(info) as usize) & (core::mem::align_of::<T>() - 1) == 0);
        self.address_with(info) as *const T
    }

    /// Returns a imutable pointer of a `Register` object.
    ///
    /// It is a simpler function when `AdditionalInfo` is default.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `T`
    fn as_ptr<T: Copy>(&self) -> *const T
    where
        Self: Register<AdditionalInfo = ()>,
    {
        assert!((self.address() as usize) & (core::mem::align_of::<T>() - 1) == 0);
        self.address() as *const T
    }

    /// Returns a imutable pointer of a `Register` object.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `T`
    fn as_mut_ptr_with<T: Copy>(&self, info: Self::AdditionalInfo) -> *mut T {
        assert!((self.address_with(info) as usize) & (core::mem::align_of::<T>() - 1) == 0);
        self.address_with(info) as *mut T
    }

    /// Returns a mutable pointer of a `Register` object.
    ///
    /// It is a simpler function when `AdditionalInfo` is default.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `T`
    fn as_mut_ptr<T: Copy>(&self) -> *mut T
    where
        Self: Register<AdditionalInfo = ()>,
    {
        assert!((self.address() as usize) & (core::mem::align_of::<T>() - 1) == 0);
        self.address() as *mut T
    }

    /// Read the register as `u8`.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u8`
    fn read_as_u8_with(&self, info: Self::AdditionalInfo) -> u8 {
        unsafe { self.as_mut_ptr_with::<u8>(info).read_volatile() }
    }

    /// Read the register as `u8`.
    ///
    /// It is a simpler function when `AdditionalInfo` is default.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u8`
    fn read_as_u8(&self) -> u8
    where
        Self: Register<AdditionalInfo = ()>,
    {
        unsafe { self.as_mut_ptr::<u8>().read_volatile() }
    }

    /// Read the register as `u16`.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u16`
    fn read_as_u16_with(&self, info: Self::AdditionalInfo) -> u16 {
        unsafe { self.as_mut_ptr_with::<u16>(info).read_volatile() }
    }

    /// Read the register as `u16`.
    ///
    /// It is a simpler function when `AdditionalInfo` is default.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u16`
    fn read_as_u16(&self) -> u16
    where
        Self: Register<AdditionalInfo = ()>,
    {
        unsafe { self.as_mut_ptr::<u16>().read_volatile() }
    }

    /// Read the register as `u32`.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u32`
    fn read_as_u32_with(&self, info: Self::AdditionalInfo) -> u32 {
        unsafe { self.as_mut_ptr_with::<u32>(info).read_volatile() }
    }

    /// Read the register as `u32`.
    ///
    /// It is a simpler function when `AdditionalInfo` is default.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u32`
    fn read_as_u32(&self) -> u32
    where
        Self: Register<AdditionalInfo = ()>,
    {
        unsafe { self.as_mut_ptr::<u32>().read_volatile() }
    }

    /// Write to the register considering the register size as `u8`.
    ///
    /// # Panics
    ///
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u8`
    ///
    /// # Safety
    /// This function can write any value to the register while most of the time the
    /// system expects a specific value. Writing a value to the register that the hardware
    /// does not understand or expect can lead to undefined behavior.
    unsafe fn write_as_u8_with(&self, info: Self::AdditionalInfo, val: u8) {
        self.as_mut_ptr_with::<u8>(info).write_volatile(val);
    }

    /// Write to the register considering the register size as `u8`.
    ///
    /// It is a simpler function when `AdditionalInfo` is default.
    ///
    /// # Panics
    ///
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u8`
    ///
    /// # Safety
    /// This function can write any value to the register while most of the time the
    /// system expects a specific value. Writing a value to the register that the hardware
    /// does not understand or expect can lead to undefined behavior.
    unsafe fn write_as_u8(&self, val: u8)
    where
        Self: Register<AdditionalInfo = ()>,
    {
        self.as_mut_ptr::<u8>().write_volatile(val);
    }

    /// Write to the register considering the register size as `u16`.
    ///
    /// # Panics
    ///
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u16`
    ///
    /// # Safety
    /// This function can write any value to the register while most of the time the
    /// system expects a specific value. Writing a value to the register that the hardware
    /// does not understand or expect can lead to undefined behavior.
    unsafe fn write_as_u16_with(&self, info: Self::AdditionalInfo, val: u16) {
        self.as_mut_ptr_with::<u16>(info).write_volatile(val);
    }

    /// Write to the register considering the register size as `u16`.
    ///
    /// It is a simpler function when `AdditionalInfo` is default.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u16`
    ///
    /// # Safety
    /// This function can write any value to the register while most of the time the
    /// system expects a specific value. Writing a value to the register that the hardware
    /// does not understand or expect can lead to undefined behavior.
    unsafe fn write_as_u16(&self, val: u16)
    where
        Self: Register<AdditionalInfo = ()>,
    {
        self.as_mut_ptr::<u16>().write_volatile(val);
    }

    /// Write to the register considering the register size as `u32`.
    ///
    /// # Panics
    ///
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u32`
    ///
    /// # Safety
    /// This function can write any value to the register while most of the time the
    /// system expects a specific value. Writing a value to the register that the hardware
    /// does not understand or expect can lead to undefined behavior.
    unsafe fn write_as_u32_with(&self, info: Self::AdditionalInfo, val: u32) {
        self.as_mut_ptr_with::<u32>(info).write_volatile(val);
    }

    /// Write to the register considering the register size as `u32`.
    ///
    /// It is a simpler function when `AdditionalInfo` is default.
    ///
    /// # Panics
    /// This function may panic if output of `Register::address` has not the same
    /// alignment for `u32`
    ///
    /// # Safety
    /// This function can write any value to the register while most of the time the
    /// system expects a specific value. Writing a value to the register that the hardware
    /// does not understand or expect can lead to undefined behavior.
    unsafe fn write_as_u32(&self, val: u32)
    where
        Self: Register<AdditionalInfo = ()>,
    {
        self.as_mut_ptr::<u32>().write_volatile(val);
    }
}
